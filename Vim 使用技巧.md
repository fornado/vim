# Vim 实用技巧

***

[toc]

---



## 前言

- 名称描述符约定

| π“‘[]dap        | [lgkkk,hj45e]                           |
| :-------------- | :-------------------------------------- |
| <C-n>           | 同时按<Ctrl>和n                         |
| g<C-]>          | 先按g，然后同时按<Ctrl>和 ] 键          |
| <C-w><C-=>      | 先同时按<C-w>，然后同时按<C-=>          |
| f{char}         | 按f，后面跟任意字符                     |
| m{a-zA-Z}       | 按m，后面跟任意小写或者大写字母         |
| d{motion}       | 按d，后面跟任意动作命令                 |
| <C-r>{register} | 同时按<Ctrl>和 r， 后面跟一个寄存器地址 |
| <Esc>           | 按退出键                                |
| <Ctrl>          | 按控制键                                |
| <S-Tab>         | 同时按<Shift>`和`<Tab>aaaa              |
| $               | 在外部shell中执行命令行命令             |
| :               | 用命令行模式执行一条ex命令              |
| /               | 用命令行模式执行正向查找                |
| ?               | 用命令模式执行反向查找                  |
| =               | 用命令行模式执行一个vim脚本表达式求值   |

- 恢复出厂设置

  `vim -u NONE -N ` or `vim -u your_vimrc`

- 查看vim的编译选项 

  `: version`

  

## 第一章 Vim 解决问题的方式

  **<u>. 范式</u>**：**用一键移动，另一键执行**

  **口决**：==执行、重复、回退==

> 面对重复性的工作时，**我们需要让移动动作和修改都能够重复**，这样就可以达到一个最佳的编辑模式。

  *可重复的操作及如何回退*

| 目的           | 操作                            | 回退 | 回退 |
| -------------- | :------------------------------ | ---- | ---- |
| 做出修改       | {edit}                          | .    | u    |
| 行内查找       | f{char}/t{char}/F{char}/T{char} | ;    | ,    |
| 文档内查找     | /pattern<CR> or ?pattern<CR>    | n    | N    |
| 执行替换       | :s/target/replacement           | &    | u    |
| 执行一系列修改 | qx{changes}q                    | @x   | u    |

注意：尽可能的使用可重复的移动和修改命令，使得可以简单的重复执行和回退。

---



## 第二章 普通模式

- **<u>什么是修改</u>**： 简单说就是从进入插入模式到退出该模式期间的所有操作视为一次修改，一般在一个 <u>可撤销块</u> 完成后退出插入模式。

- **如何构建可重复的修改**：尽可能少的按键次数和使用`.` 命令可以达到想要的效果。比如当光标位于所要删除的单词的最后一个字符上面，使用`daw` 比 `dbx`和`bdw` 跟合适，后两者使用 `.`命令时没多大用。

- 使用`<C-a>` or `<C-x>` 来巧妙的移动到和操作行内的数字字符。

- 能够重复，就别用次数。比如 删除连续的多个单词，使用 `dw.` 而不是 `2dw` or `d2w`, 只在必要的时候使用 次数。

- **操作符 + 动作命令 = 操作**

  - d{motion} 命令可对一个字符 `dl`、一个单词 `daw`、一个段落 `dap` 进行操作。
  - 其他如c{motion}, y{motion}等。

  Vim 的操作符命令

  | 命令 | 用途                               |
  | ---- | ---------------------------------- |
  | c    | 修改                               |
  | d    | 删除                               |
  | y    | 复制到寄存器                       |
  | g~   | 反转大小写                         |
  | gu   | 转换为小写                         |
  | gU   | 转换为大写                         |
  | >    | 增加缩进                           |
  | <    | 减小缩进                           |
  | =    | 自动缩进                           |
  | !    | 使用外部程序过滤{motion}所跨越的行 |

  另外我们还可以自定义*操作符* 和 *动作命令* 来扩展Vim。

  > *操作符待决模式*： 它在输入操作符后等待随后输入的动作命令。

---



## 第三章 插入模式

- 在插入模式中可即时更正错误。使用退格、`<C-h>`、`<C-w>`、`<C-u>`
- 返回普通模式。使用 <Esc> 、<C-]> 、<C-o>
- **插入普通模式**： 使用 `<C-o>` 进入插入-普通模式，随后自动返回插入模式。
- 不离开插入模式，粘贴寄存器中的文本：`<C-r>{register}`
- 随时随地做运算。`<C-r>=`
- 用字符编码插入非常用字符。在插入模式下 `<C-v>{code}` or `<C-v>u{1234}`, 另外可以使用 `ga` 查看任意字符的编码。
- 用二合字母插入非常用字符。`<C-k>{char1}{char2}`, 如<C-k>?I, 可以查阅 `:h digraph-table`
- 用替换模式替换已有文本。使用 `R` 命令，进入替换模式， 使用 `gR`命令进入虚拟替换模式，使用 `r`命令或者 `gr` 命令短暂进入替换模式后，自动返回。
- 

---



## 第四章 可视模式

- 深入理解可视模式。
- 选择高亮选区。
  - 面向字符的可视模式 `v`
  - 面向行的可视模式 `V`
  - 面向列的可视模式 `<C-v>`
  - 重选上次的高亮选区。`gv`
  - 在可视模式间切换。`v` 模式下，按 `V`进入行可视模式，按`<C-v>`。
  - 按 `o` 切换高亮选区的活动端。
- 重复执行面向行的可视命令，使用 `.`
- **只要可能，最好用操作符命令，而不是可视命令**
  - 注意：是使用 `.` 命令重复执行可视模式命令时，它只会影响相同数量的文本，如果执行面向字符模式，可能会出现异常，因此它主要适用于面向行模式的命令, 面向字符模式的命令最好用操作符命令。
- 面向列块的可视模式边际表格数据。使用 `<C-v>` 进行块模式。
- 修改列文本。`<C-v>` 后使用操作符命令，同时作用与多行。
- 在长短不一的高亮块后添加文本。移动到首行末尾，`<C-v>jj$`即可。

---



## 第五章 命令行模式

- 结识Vim的命令行模式

  - 操作缓冲区文本的Ex命令</u>

    | 命令                                          | 用途                                                     |
    | --------------------------------------------- | -------------------------------------------------------- |
    | :[range]delete [x]                            | 删除指定范围内的行[到寄存器x中]                          |
    | :[range]yank [x]                              | 复制指定范围的行[到寄存器x中]                            |
    | :[line]put [x]                                | 在指定行后粘贴寄存器x中的内容                            |
    | :[range]copy {address}                        | 把指定范围内的行拷贝到{address}所指定的行之下            |
    | :[range]move {address}                        | 把指定范围内的行移动到{address}所指定的行之下            |
    | :[range]join                                  | 连接指定范围内的行                                       |
    | :[range]normal {commands}                     | 对指定范围内的每一行执行普通模式命令{commands}           |
    | :[range]substitute/{pattern}/{string}/[flags] | 把指定范围内出现{pattern}的地方替换为{string}            |
    | :[range]global/{pattern}/[cmd]                | 对指定范围内匹配{pattern}的所有行，在其上执行Ex命令{cmd} |

    - Vim命令行模式有一些特殊按键，大部分与插入模式相同，如`<C-w>`, <C-u>, <C-v>, <C-k>, <C-r>...

    - Ex命令影响范围广并且距离远，它可以在任意位置执行而不需要移动光标，它能在一次执行中修改多行；

      

- 在一行或多个连续行上执行命令

  - 用行号作为地址。如 `:1`, `:3p`

  - 用地址指定一个范围`:{start},{end}`，如`:2,5p`, `.,$p`, `:%s/pattern/replacement`

  - 用高亮选区指定范围。使用`VG` 选中区域，然后 `:`命令会自动填充之前选择的范围。

  - 用模式指定范围。常用搜索命令 `:/pattern1,/pattern2/p`

  - 用偏移对地址进行修正, `:{address}+n`。如 `:/pattern1+1,/pattern/-1p`, `:.,.+3p`

    <u>Ex命令的地址和范围的符号</u>

    | 符号 | 地址                       |
    | ---- | -------------------------- |
    | 1    | 文件的第一行               |
    | $    | 文件的最后一行             |
    | 0    | 虚拟行，位于文件第一行上方 |
    | .    | 光标所在行                 |
    | 'm   | 包含位置标记m的行          |
    | '<   | 高亮选区的启始行           |
    | '>   | 高亮选区的结束行           |
    | %    | 整个文件(:1,$)的简写形式   |

  

  - 使用':t' 和 ':m' 命令复制和移动行

    - `[range]t{address}`, 将range范围内的文本粘贴到address处，注意与`yyp`不同，`:t` 不使用寄存器。
    - `[range]move{address}`, 将range范围内的文本移动到address处。
    - 重复上次的Ex命令，使用`@:`

  - 在指定范围上执行普通模式命令

    - 在一系列连续行上执行一条普通模式命令，使用 `:[range]normal`, 它可以和`.`命令或宏结合使用，完成大量重复性任务，如 `:'<,'>normal .`, `:[range]normal @q`

  - 重复上次的Ex命令

    - 使用`@:`重复执行上一次命令，随后可使用`@@`来重复。

  - 自动补全Ex命令

    - 使用`<Tab>`
    - 使用`<C-d>`显示可用的补全列表

  - 把当前单词插入到命令行

    - 使用`<C-r>` `<C-w>` 映射项会复制光标下的单词并把它插入到命令行中。比如，修改光标下的单词，先按`*`,然后修改为 replacement，其他地方用 `:s//<C-r><C-w>/g`来替换。
    - 另外可以使用 `<C-r><C-a>` 插入字串。

  - 回溯历史命令

    命令历史不仅仅始为当前编辑会话记录的，这些历史即使退出Vim再重启之后依然存在。

    - 输入 `:` or `:{命令的一部分}` 来查询历史命令

    - 使用`set history=200` 设置历史命令记录数量

    - Vim还会保存查找命令历史记录，输入 `/` 上下翻可以查询

    - 上下翻，可以使用`<Up>`,`<Down>` 或者 `<C-p>`, `<C-n>`

      > <Up> 和 <Down>会对输入的命令进行过滤，<C-p> 和 <C-n>不会，一般我们需要修改映射。
      >
      > ```vim
      > cnoremap <C-p> <Up>
      > cnoremap <C-n> <Down>
      > ```

    - 或者 输入`q:` 打开命令历史窗口进行操作, 比如修改命令历史记录等。

  - 运行shell命令

    - 常用的符号
      - % 表示当前的文件名，其他文件相关的 参考 `:h filename-modifiers`
    - 可以使用 `!{cmd}`来执行一次性的命令
    - 还可以使用 `:shell`来启动一个交互的shell会话
    - 把缓冲区内容作为标准输入或输出
      - 使用 `!{cmd}`时，Vim会回显{cmd}命令的输出
      - 使用 `:read !{cmd}` 把{cmd}命令的输出读入到当前缓冲区中。
      - 使用 `:write !{cmd}` 则相反，它把缓存区内容作为指定{cmd}的标准输入。
    - 使用外部命令过滤缓冲区内容
      - `[range]!{filter}` 使用外部程序{filter}过滤指定的[range]
      - 可以使用`!{motion}` 操作符切换到命令行模式，它会把{motion}所覆盖的范围预置在命令行上。

  

  ---

  

  ## 第六章 管理多个文件

  - 用缓冲区列表管理打开的文件

    - `ls` 显示当前缓存区列表，包含已被加载到内存的文件列表

      > `%` 代表该缓冲区在当前窗口可见，`#` 代表轮换文件，可以使用`<C-^` 快速切换到该文件。

    - `:bnext` or `:bprev` 切换缓存区

    - `:bfirst` or `:blast` 分别跳到列表的开头和结尾。

    - `:buffer N` 命令跳转到编号为N的缓冲区

    - `bufdo` 命令允许我们在`:ls` 列出的所有缓冲区上执行Ex命令。

      > 创建快速遍历Vim列表的按键映射项

      ​		

      ```
      nnoremap <silent> [b :bprevious<CR>
      nnoremap <silent> ]b :bnext<CR>
      nnoremap <silent> [B :bfirst<CR>
      nnoremap <silent> ]B :blast<CR>
      另外遍历参数列表可以使用 [a 和 ]a, quickfix列表([q 和 ]q), 位置列表（[l 和 ]l），标签列表([t 和 ]t)
      ```

    - 删除缓存区

      `:bdelete n1 n2 n3` or `:n,m bdelete`

  - 用参数列表将缓冲区分组

    - `:args` 命令显示启动时作为参数传递给Vim的文件列表。
    - 填充参数列表 `:args {arglist}`
      - 使用文件名 `:args index.html app.js`
      - 使用Glob模式指定文件
        - `*` 符号用于匹配0个或多个字符，但它范围仅限于指定的目录，而`不会递归`其子目录
        - `**`通配符也匹配0个或多个字符，但可以`递归`进入目录的子目录
        - 例子，`:args *.*`, `:args **/*.js`, `:args **/*.*`
      - 用反引号结果指定文件
        - `:arg` \`cat .chapters\`
      - 使用参数列表
        - 使用 `:next` 以及 `:prev` 遍历参数列表中的文件
        - 使用 `:argdo`命令在列表中的每个缓冲区上执行同一个命令

  - 管理隐藏缓冲区

    - `ls` 命令显示 #h 表示隐藏缓冲区，主要当文件修改未保存时，强制切换`:bn!`到其他缓存区导致当前文件被放置到隐藏缓冲区。
    - 可以在退出vim时处理隐藏缓冲区。使用`:w` or `:edit!` 来保存或者丢弃修改，也可`:qall!`丢弃所有修改的缓冲区并退出Vim，或者 相反 `:wall` 保存所有缓冲区。
    - 可以开启`hidden`选项，避免必须输入感叹号来执行`:next`命令的场合。开启后，vim会在离开该缓冲区时自动将其设置为隐藏，适用于 `:argdo {cmd}`场景。

  - 将工作区切分成窗口

    - 使用`<C-w>s` 将当前窗口水平切分

    - 使用 `<C-w>v`将当前窗口垂直切分

    - 使用上面切分窗口后，再执行`:e{filename}` 可以将新窗口显示新的缓冲区, 

    - 也可以使用 `:[v]split{filename}`将两步合成一步。

    - 在窗口间切换

      - `<C-w>{w,hjkl}

    - 关闭窗口

      - `:clo[se]` 或者 `<C-w>c`
      - `:on[ly]` 或者 `<C-w>o`

    - 改变窗口大小以及重新排列窗口

      | 命令       | 用途                    |
      | ---------- | ----------------------- |
      | <C-w>=     | 使所有窗口等宽等高      |
      | <C-w>_     | 最大化活动窗口的高度    |
      | <C-w>\|    | 最大化活动窗口的宽度    |
      | [N]<C-w>_  | 把活动窗口的高度设为N行 |
      | [N]<C-w>\| | 把活动窗口的宽度设为N列 |

      > 窗口重排参考 `:h window-moving`

      

  - 用标签页将窗口分组

    标签页时可以容纳一系列窗口的容器，参见 `:h tabpage`

    - 标签页一般用户划分不同的工作区域, 
    - 使用 `:lcd {path}`来设置新标签页当前窗口的本地工作目录。
    - 使用 `:tabe[dit] {filename}` 创建新的标签页并打开filename
    - 使用 `<C-w>T` 把当前窗口移动到一个新的标签页
    - 使用 `:tabc[lose]` 关闭当前标签页以及其中的所有窗口
    - 使用`tabo[nly]` 只保留活动标签页，关闭所有其他标签页

  - 在标签页间切换

    | Ex命令         | 普通模式命令 | 用途                    |
    | -------------- | ------------ | ----------------------- |
    | :tabn[next]{N} | {N}gt        | 切换到编号为{N}的标签页 |
    | :tabn[next]    | gt           | 切换到下一个标签页      |
    | :tabp[next]    | gT           | 切换到上一个标签页      |

    

  - 重排标签页

    `:tabmove[N]`命令可以重排标签页。

  ---

  

  ## 第七章 打开及保存文件

  - 使用 `:e[dit]{filepath}` 命令打开文件

    - 相对于当前工作目录打开一个文件

    - 相对于活动文件目录打开一个文件

      输入`:e[dit] %:h<Tab>{filename}`, 可以方便的把活动文件的目录展开，然后再输入其他文件路径。

      > 增加映射%:h , 再命令行模式下输入连续两个 `%%`，会自动展开当前活动文件全路径。
      >
      > ```
      > cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'
      > ```

  - 使用 `:find`打开文件

    - 该命令的好处，相比`:e[dit]`命令，它不需要输入完整路径，前提需要配置好`path`选项
    - 配置 `path`选项，`:set pat+=app/**`

  - 使用netrw管理文件系统

  - 把文件保存到不存在的目录中

    - `<C-g>` 命令用于显示当前文件的文件名及状态
    - `:e notexsitPath/filename` 编辑不存在的目录文件时，在保存之前调用`:!mkdir -p %:h`创建目录。

  - 以超级用户权限保存文件
    - `:w !{cmd}`  {cmd} 参见`:h :write_c`
    - `:w !sudo tee % > /dev/null`

  ---

  

  ## 第八章 用动作命令在文档中移动

  Vim拥有大量动作命令，参见`:h motion.txt`

  - 让手指保持在 `h,l,j,k`按键上

  - 区分实际行与屏幕行，使用 `gj` or `gk` 按屏幕行向下或者向上移动，`g0`, `g^`,`g$`移动到屏幕行首、第一个非空白字符、行尾

  - 基于单词移动

    - `w` 移动到下一个单词的开头
    - `b` 移动到当前单词或者上一个单词的开头
    - `e` 移动到当前单词或者下一个单词的结尾
    - `ge`反向移动到上一个单词的结尾
    - 单词和字串
      - 单词是由字母、数字、下划线或其他非空白字符的序列组成，它以*空白字符*分隔，移动命令有 `w`, `b`, `e`, `ge`
      - 字串是由非空白字符序列组成，移动命令有 `W`, `B`, `E`, `gE`

  - 对字符进行查找

    - 使用`f{char}` or `t{char}`正向查找 或者 使用`F{char}` or `T{char}`反向查找
    - 使用`;` or `,`重复执行查找，并跳转到其他搜索结果
    - 一般的在普通模式中使用`{f,F}{char}`,在操作符待决模式中使用 `{t,T}{char}`
    - 最好是选择出现频率比较低的字符作为目标字符，提高查找效率。

  - 通过查找进行移动

    - 开启`incsearch`选项，使用 `/{char}`和`n`, `N`命令查找移动
    - **用查找动作操作文本，在其他模式下使用查找命令，定位文本, 比如 v模式、操作符待决模式。**, 如`d/ge<CR>` , `v/ge<CR>d`

  - 用精确的文本对象选择选区

    - 文本对象 是基于结构定义的文本区域，通过使用文本对象，可以很方便的选择和操作一大段文本
    - 常用的有 `{a,i}{(,},",',>,t,],}`
    - **注意：可以直接结合 c{motion} 直接在操作服务待决模式中使用，不需要进入visual模式。**

  - 删除周边，修改内部

    - `iw` inside word, `aw` around the word.
    - 看情况使用 inside or around

  - 设置位置标记，以便快速跳回

    - 使用`m{a-zA-Z}`设置一个位置标记, 小写只在每个缓冲区局部可见，大写位置标记全局可见。

    - 使用``{mark}` 跳转到位置标记处。

    - Vim

      | ``   | 当前文件中上次跳转动作之前的位置 |
      | ---- | -------------------------------- |
      | `.   | 上次修改的地方                   |
      | `^   | <u>上次插入的地方</u>            |
      | `[   | <u>上次修改或复制的起始位置</u>  |
      | `]   | 上次修改或复制的结束位置         |
      | `<   | <u>上次高亮选区的位置</u>        |
      | `>   | 上次高亮选区的位置               |

      

  - 在匹配括号间跳转

    - 在成对的 符号间跳转，使用`%`, 比如 `{aaa}`

    - 注意：**使用%，Vim会自动生成一个位置标记，我们可以随后使用 ``回到跳转之前的位置**

    - 在配对的关键字间跳转

      > Tim Pope写的 surround.vim 很实用这种场景。

  ---

## 第九章 在文件间跳转

- 遍历跳转列表

  任何改变当前窗口中活动文件 命令都可以称为 `跳转命令`， 每个窗口都有一份跳转列表。

  > Vim 会把执行跳转命令之前和之后的光标位置记录到跳转列表中。比如运行`:e[dit]`命令打开一个新文件，那么可以使用`<C-o>` 和 `<C-i>`命令在新文件和原文件间跳转。

  - [count]G、面向句子和段落的动作都算跳转。

    常见的跳转动作

    | 命令                          | 用途                             |
    | ----------------------------- | -------------------------------- |
    | [count]G                      | 跳转到指定的行号                 |
    | /pattern<CR>/?pattern<CR>/n/N | 跳转到下一个、上一个模式出现之处 |
    | %                             | 跳转到匹配的括号所在之处         |
    | ( / )                         | 跳转到上一句、下一句的开头       |
    | { / }                         | 跳转到上一段、下一段的开头       |
    | H/M/L                         | 跳转到屏幕最上方、正中间、最下方 |
    | gf                            | 跳转到光标下的文件名             |
    | <C-]>                         | 跳转到光标下关键字的定义之处     |
    | \`{mark} / \`{mark}           | 跳转到一个位置标记               |

    

- **遍历改变列表**

  **每当对文档做出修改后，Vim都会记录当前光标所在位置，遍历改变列表可能是跳到要去的地方最块的方式, 注意 它只记录了当前激活的文档的修改记录，如果是其他文件，需要使用 <u>跳转列表</u>。**参见：`:h changelist`

  - **输入 `:changes` 查看改变列表**
  - **输入：`g;` or `g,` 反向或正向遍历改变列表**
  - 另外可以使用 `. or `^ 位置标记跳转到之前修改和退出插入模式的地方，比如 `gi`

- **跳转到光标下的文件**

  - 在进行正确的配置后，使用 `gf` 跳转到光标下的文件。
  - 指定文件的扩展名
    - `suffixeaadd`选项允许我们指定一个或多个文件扩展名，当使用`gf`命令搜寻文件名时，会尝试使用这些扩展名。`:set suffixesadd+=.rb`
    - 每次使用`gf`命令时，V    im都会跳转列表中添加一条记录，我们可以很方便的使用`<C-o>`返回。

- 用全局位置标记在文件间快速跳转

  - 使用`m{A-Z}`和`\`{A-Z}` 设置和跳转到位置标记
  - 一般在使用与 <u>quickfix</u>（grep，vimgrep，make）列表相关的命令，执行与缓冲区列表或参数列表有关的命令前（args {arglist}, :argdo) 设置全局标记。

---



## 第10章 复制与粘贴

- 使用无名寄存器实现删除、复制与粘贴操作

- 深入理解Vim寄存器

  - 引用寄存器：`"{register}` 如`"ayiw`, `"ap`, `:{cmd} {register}`

  - 无名寄存器 `""`

  - 复制专用寄存器 `"0`

  - 有名寄存器 "{a-zA-Z

    > 注意使用小写字母引用寄存器会覆盖该原有内容，使用大写字母则时追加到原有内容之后。

  - 黑洞寄存器 "_, 它不保存任何副本，适用于删除文本却不想覆盖其他寄存器的内容的场景。

  - 系统剪切板 `"+`

  - 选择专用寄存器 `"*`

  - 表达式寄存器 `"=`

  - 其他只读寄存器 `"%`, `"#`, `".`, `":`, `"/`

- 用寄存器中的内容替换高亮选区的文本

  - 首先 `yiw` 在v模式选中对应的区域后，输入`p` 命令粘贴，注意: yiw复制的内容保存到了复制专用寄存器，输入p命令后，修改了无名寄存器的内容为被替换的内容，此后在输入p，会输出 "replacement"。
  - 替换两个词。删除前一个单词后，原地标记，替换后一个单词后返回标记直接p；`demmvep`mP`

- 把寄存器中的内容粘贴出来

  - 粘贴面向字符的区域

    插入模式使用 `<C-r>{register}`

  - 粘贴面向行的区域

    > `p` or `P`, 另外 `gp` or `gP`, 后两者会把光标的位置移动被粘贴出来的文本的结尾而不是开头，当复制多行时，**`gP`命令尤为管用**。

    一般粘贴多行文本区域时使用 `p`命令，而面向字符的文本，可以考虑使用`<C-r>{register}`

- 与系统剪切板进行交互

  - 第一种方式，在插入系统剪切版之前 启用`paste`选项，粘贴完了后重新关闭这个选项；
  - 另一种方式，使用`"+`号寄存器

---

## 第11章 宏

​	宏很适合针对一系列相似的行、段落，甚至文件，进行重复性的修改。它有两种执行方式：串行和并行。	