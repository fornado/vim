# amid/vimrc 使用技巧

## 主要组件

## 常用按键

### Normal

2. 缓存区列表导航：**bufexplorer.zip** (`<leader>o`)

3. 模糊查询文件，缓存区、最近修改、tag：**ctrlp.vim**(`<C+F>`)

4. 专注编辑状态：**goyo.vim**, **vim-zenroom2**(`<leader>z`)

5. 目录树：**NERD Tree**(`<leader>nn`, `<leader>nb`, <leader>nf)

6. 最近修改文件列表：**mru.vim**(`<leader>f`)

7. 打开光标下单词的文件：**open_file_under_cursor.vim**(`gf`)

8. 添加注释：**vim-commentary**(`gcc`, `gc`, `gcu`)

9. 保存缓冲区：`<leader>w`

10. 搜索当前缓冲区： `<space>`

11. 取消搜索高亮：`:noh<cr>`

12. 切换windows：`<C-j>`, `<C-k>`, `<C-h>`, `<C-l>`

13. 关闭当前缓冲区：`<leader>bd` , `<leader>ba`

14. **tab相关：**
    1. `<leader>tn`, `<leader>to`, `<leader>tc`, `<leader>tm`
    2. `<leader>te`: Opens a new tab with the current buffer's path
    3. 切换tab：`:tabn [{count}]` or `gt` or `gT`

15. **切换进入到当前缓冲区目录：`<leader>cd`**

16. 切换paste设置： `<leader>pp`

17. 跳转到其他语法检查错误处： `<leader>a`

18. 拼写检查：`<leader>ss`

### Visual mode

1. 使用ack.vim查询选中的内容：`<leader>gv`
2. **当前缓冲区替换选中内容：`<leader>r**`
3. Surround：`$1`(), $2 [], $3 {}, $$ "", $q '', $e ""

### Insert mode

1. Inoremap $1 (), $2 [], $3 {}, $4 { }, $q '', $e "", $t <>
2. 插入时间戳：`iab xdate`
3. 

### 常用的插件

#### Ack.vim 搜索关键字

1. 主要功能：搜索关键字和文件
2. 主要命令：
   1. :[L]Ack[!] [options] {pattern} [{directories}] 在`directories`中递归查找符合`pattern`模式的内容，并显示到`quick-fixlist` or `location-list`中, 加`!`表示不跳转到第一个结果
   2. **:[L]AckWindow [option] {pattern}** 仅仅在当前可见的buffer对应的文件中查找符合模式的结果
   3. :[L]AckAdd [options] {pattern} [{directories}] 追加搜索结果
   4. :AckFromSearch [{directory}] 按照上一次搜索的模式检索
   5. :AckFile [options] {pattern} [{directory}] 递归查询`directory`目录下符合`pattern`的文件名列表。
   6. `<leader>g [option] {pattern}[{directory}]`
3. 常用mapping，见`:help Ack` 
   1. **t: 打开当前选中的结果，并新建tab并切换到新tab**
   2. T: 同上，但是不自动切换到新tab
   3. o: 打开在当前窗口打开选中的结果
   4. **O: 同上，并关闭`quickfixlist` or `location-list**`
   5. go: 在当前窗口新建垂直分割显示当前选中的结果。
   6. h: 在水平分割中打开
   7. H: 同h，但是不移动光标
   8. v: 在垂直分割中打开，并切换光标
   9. gv: 同上, 光标仍然保持在结果列表缓存区
   10. **`n` or `p` 不移动光标位置，直接显示下一个或者后一个内容**
4. 常用选项
   1. let g:ackhighlight = 1 设置搜索内容高亮
   2. let g:ack_autofold_results = 1 根据文件名自动折叠搜索结果，只有当前fold会打开，当按`j` or `k`移动时，自动关闭其他折叠，打开当前折叠
   3. let g:ackpreview = 1 设置在结果列表移动时，自动打开。
   4. let g:ack_use_dispatch = 1 后台搜索。

#### bufexplorer.zip 缓冲区导航

1. 主要功能：缓冲区导航，快速容易的切换buffers
2. 主要命令：
   1. `:BufExplorer` 打开缓冲区切换界面
   2. `:ToggleBufExplorer` 打开或者关闭缓冲区切换界面
   3. `:BufExplorerHorizontalSplit` 水平分割区显示切换界面
   4. `:BufExplorerVerticalSplit` 垂直分割区显示切换界面
   5. **`<leader>o`** 打开缓冲区导航
   6. **`q` 退出缓冲区导航**
   7. `b` 进入BufExplorer后，输入`b[buffernum]`进入切换到对应的buffer
   8. `d` 删除当前缓冲区记录
   9. **`F` or `f` or `V` or `v` 打开选中的buffer，置于另一个窗口在当前窗口之上或者之下或者左边或者右边**
   10. `o` 在当前窗口打开buffer
   11. `p` 切换显示文件名和目录名称
   12. `r` 翻转缓冲区列表的显示顺序
   13. `R` 切换显示缓冲区的相对和绝对路径
   14. `s` or `S` 循环切换缓存区列表排序方式，包括序号、文件名、文件类型、MRU、全路径等
   15. **`t`在另一个tab中打开当前缓存区**
   16. **`T` 切换是否只显示当前tab的缓存区列表**
   17. `u` 切换是否显示"unlisted" 缓冲区



#### ctrlp.vim 模糊查找

1. 主要功能：模糊查找文件、缓存区、最近修改和tag（`<Ctrl+F>` ）
2. 主要命令：
   1. `<C-j>` or `<C-d>` 上下浏览结果
   2. `<C-c>` or `<C-g>` 退出搜索
   3. `<cr>` 跳转到当前tab的窗口内
   4. `<c-t>` 新建tab显示
   5. **`<C-v>` or `<C-x>` 垂直分割或者水平分割显示**
   6. `:CtrlP` 使用文件搜索模式打开
   7. **`:CtrlPBuffer` 使用缓冲区搜索模式打开**
   8. `:CtrlPClearCache` or `:CtrlPClearAllCaches` 删除缓存
3. 主要映射
   1. `<C-d>` 切换文件名搜索和全路径搜索模式
   2. `<C-r>` 切换字符串搜索模式和正则表达式搜索模式
   3. `<C-y>` 创建一个新文件和它的父目录



#### NERD Tree 目录树

1. 主要功能：目录树
2. 主要命令：
   1. `:NERDTree[<start-directory>|<bookmark>]`
   2. `:NERDTreeVCS[<start-directory>|<bookmark>]` 同上，但是会搜索版本库根目录。
   3. **`:NERDTreeFromBookmark <bookmark>` or  `<leader>nb`以书签作为根目录打开**
   4. `:NERDTreeToggle[VCS] [<start-directory> | <bookmark>]`  切换
   5. `Bookmark [<name>]` 创建书签 or 
   6. `OpenBookmark <name>` 打开书签
   7. `ClearBookmarks [<bookmarks>]` or `D` 删除书签
   8. `o` or `go` 打开文件，后者保持光标在目录树
   9. `t` or `T` 在新标签中打开文件，后者不移动
   10. `i` or `gi` 在水平分割区显示
   11. `s` or `gs` 在垂直分割区显示
   12. **`O`** or **`X`** 递归打开或者关闭当前目录下的所有子目录
   13. `e`在分区中打开新的目录树
   14. `p` 跳转到父节点 `P` 跳转到树的根节点
   15. `K` or `J` 跳转到当前父节点的第一个或者最后一个子节点
   16. `<C-j>` or `<C-k` 同级别的节点前面移动
   17. **`C` 设置当前节点为根节点**
   18. **`u` 移动到根目录**
   19. **`r` or `R`  刷新当前节点或者根节点**
   20. `q` 关闭
   21. **`CD`** 切换根目录到cwd
   22. **`cd`** 切换cwd到打开的文件所在的目录
   23. **`<leader>nf` or `:NERDTreeFind` 切换光标到NERDTree**

#### mru.vim 管理最近使用文件列表

1. 主要功能：管理最近使用文件列表 （<leader+f>)
2. 主要命令：
   1. `o` or `<Shift-Enter>` 在水平分割区或者新窗口中打开文件
   2. `v` 以只读模式打开文件
   3. `t` 在新标签页中打开文件
   4. 支持面向行选择模式，同时打开多个文件
   5. `u` 更新最近使用文件列表，在mru长开的情况比较有用。
   6. `q` 退出MRU
   7. `:MRU [filenamepattern]` 只显示最近使用的文件名称符合该模式的文件列表
   8. `let MRU_Exclude_Files = {pattern}` or `let MRU_Include_Files = {pattern}` 设置不显示和显示的文件模式

#### Snipmate.vim 内容片段管理

1. 主要功能：通过输入部分内容和触发键进行扩展内容
2. 主要命令：
   1. 默认触发键 `<tab>` , 见`SnipMate-mappings`
   2. `:SnipMateOpenSnippetFiles` 打开基于当前 scope 的所有有效的spnippet 位置, scope会根据文件类型和语法自动加载，如我们打开filetype为objc的文件，当前scope就时objc.snippet
   3. `:SnipMateLoadScope[!] scope [scope...]` 加载snippets 从另外的scopes
   4. `[is]map <C-J> <Plug>snipMateNextOrTrigger` 自定义触发键



#### ale.vim 语法检查

1. 主要功能：语法检查、自动补全、跳转
2. 主要命令：
   1. `<leader>a` 跳转到下一个语法检查错误
   2. 

#### vim-surround.vim

1. 主要功能：提供包裹字符串的功能
2. 主要命令：
   1. vmap Si S(i_<esc>f)