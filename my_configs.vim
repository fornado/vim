
set number

" vim-swift
"let g:syntastic_swift_checkers = ['swiftpm', 'swiftlint']
"let g:syntastic_swift_checkers = ['swiftpm']

" vim-syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

map <leader>ct :cd ~/Desktop/Todoist<cr>

" ack
let g:ackhighlight = 1
let g:ack_autofold_results = 1
let g:ackpreview = 1

" MRU
let MRU_Auto_Close = 0

" Snipmate
"imap <C-J> <Plug>snipMateNextOrTrigger
"smap <C-J> <Plug>snipMateNextOrTrigger
"imap <C-K> <Plug>snipMateBack

" ale
" lightline-ale
"let g:lightline = {}
"
"let g:lightline.component_expand = {
"      \  'linter_checking': 'lightline#ale#checking',
"      \  'linter_infos': 'lightline#ale#infos',
"      \  'linter_warnings': 'lightline#ale#warnings',
"      \  'linter_errors': 'lightline#ale#errors',
"      \  'linter_ok': 'lightline#ale#ok',
"      \ }
"
"let g:lightline.component_type = {
"      \     'linter_checking': 'right',
"      \     'linter_infos': 'right',
"      \     'linter_warnings': 'warning',
"      \     'linter_errors': 'error',
"      \     'linter_ok': 'right',
"      \ }
"
"let g:lightline.active = { 'right': [[ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_infos', 'linter_ok' ]] }

